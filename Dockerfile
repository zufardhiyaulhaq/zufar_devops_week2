FROM ruby:2.6.5-alpine3.11 as builder

RUN apk add --update --no-cache \
    build-base \
    postgresql-dev \
    nodejs \
    yarn \
    tzdata

WORKDIR /alitwitter

COPY Gemfile* ./
COPY vendor/ ./vendor/
COPY node_modules/ ./node_modules/

RUN gem install -v '2.1.4' bundler
RUN bundle install --jobs 4 --local --deployment --verbose

# Remove unneeded files (cached *.gem, *.o, *.c)
RUN rm -rf /usr/local/bundle/cache/*.gem \
 && find /usr/local/bundle/gems/ -name "*.c" -delete \
 && find /usr/local/bundle/gems/ -name "*.o" -delete

COPY package.json yarn.lock ./
RUN yarn install

COPY . ./

RUN RAILS_ENV=production SECRET_KEY_BASE=production_build bundle exec rake assets:precompile
RUN rm -rf node_modules tmp/cache vendor/assets lib/assets spec

FROM ruby:2.6.5-alpine3.11

RUN apk add --update --no-cache \
    postgresql-client \
    tzdata

RUN gem install bundler -v '2.1.4'

RUN addgroup -g 1000 -S rails \
    && adduser -u 1000 -S rails -G rails
USER rails

COPY --from=builder /usr/local/bundle/ /usr/local/bundle/
COPY --from=builder --chown=rails:rails /alitwitter /alitwitter

ENV RAILS_LOG_TO_STDOUT true
ENV RAILS_SERVE_STATIC_FILES true
ENV EXECJS_RUNTIME Disabled

WORKDIR /alitwitter

RUN date -u > BUILD_TIME

EXPOSE 8080
CMD ["bundle", "exec", "rails", "server", "-b", "0.0.0.0", "-p", "8080"]
