## Infrastructure Setup

![Infrastructure](pictures/week2-vagrant-infra.png)

Requirement:
- [Vagrant](https://www.vagrantup.com/docs/installation/)
- [Virtualbox](https://www.virtualbox.org/wiki/Downloads)
- [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)
- [minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/)
- [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- [helm 3](https://helm.sh/docs/intro/install/)

## Installation

#### Kubernetes Installation
- Create Kubernetes using minikube
```
mac$ minikube start --vm-driver='virtualbox'
```

- Check Kubernetes status
```
mac$ minikube status
mac$ kubectl get nodes
mac$ kubectl get componentstatus
```

#### Gitlab Runner Installation
- Provisioning gitlab runner
```
mac$ vagrant up
```

- Check infrastructure environment
```
mac$ vagrant status
```

This will create the following virtual machine:

| Name | IP Address | Description | Username | password |
|-|-|-|-|-|
| gitlab-runner-vm | 192.168.150.10 | Gitlab Runner VM | vagrant | vagrant |

- Generate ssh keypair in host operating system if necessary
```
mac$ ssh-keygen
```

- Copy host operating system public key to gitlab-runner-vm
```
mac$ ssh-copy-id vagrant@192.168.150.10
```

- Change configuration in `provisioning/group_vars/all.yml` if neccessary

| Config | Description |
|-|-|
| runner.url | URL for runner |
| runner.registration_token | token for runner |
| runner.shell.description | description for shell runner |
| runner.shell.tag | tag for shell runner |
| runner.docker.description | description for docker runner |
| runner.docker.tag | tag for docker runner |
| runner.docker.image | default image for docker runner |

Basically you only need to change `runner.registration_token` with registration token from `gitlab repository`.

- Change inventory in `provisioning/hosts/hosts` if neccessary
- Provisioning & preparing gitlab-runner machine
```
mac$ ansible-playbook -i provisioning/hosts/hosts provisioning/installation.yml
```

- make sure gitlab-runner is ready to use (Settings > CI / CD > Runners in gitlab), if not ready, try to verify inside gitlab-runner machine
```
mac$ ssh vagrant@192.168.150.10
```

```
vagrant$ sudo su
root# gitlab-runner verify
```

- Install kubectl & helm 3 in gitlab-runner-vm
```
mac$ ssh vagrant@192.168.150.10
```

```
vagrant$ sudo su
root# curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash
root# curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
root# chmod +x ./kubectl
root# sudo mv ./kubectl /usr/local/bin/kubectl
```

- Check kubectl & helm installed in gitlab-runner-vm
```
root# kubectl version --client
root# helm version
```

#### Kubernetes Setup
- Create `production` namespace
```
mac$ kubectl create namespace production
```

- Install postgresql with helm
```
mac$ helm install postgresql-production-database -n production -f docs/chart-postgresql/values.yaml bitnami/postgresql
```

- Apply below command to kubernetes, create production-sa service account and grant full access to production namespace.
```
mac$ cat <<EOF | kubectl apply -f -
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: production-sa
  namespace: production
---
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: production-rw-role
  namespace: production
rules:
- apiGroups: ["*"]
  resources: ["*"]
  verbs: ["*"]
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: production-rw-rolebinding
  namespace: production
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: production-rw-role
subjects:
- namespace: production
  kind: ServiceAccount
  name: production-sa
EOF
```

- Get information from service account
```
mac$ NAMESPACE=production
mac$ SERVICE_ACCOUNT=production-sa
mac$ APISERVER=$(kubectl config view --minify -o jsonpath='{.clusters[0].cluster.server}')
mac$ SECRET_NAME=$(kubectl get serviceaccount -n $NAMESPACE $SERVICE_ACCOUNT -o jsonpath='{.secrets[0].name}')
mac$ TOKEN=$(kubectl get secret -n $NAMESPACE $SECRET_NAME -o jsonpath='{.data.token}' | base64 --decode)
mac$ CA=$(kubectl get secret -n $NAMESPACE $SECRET_NAME -o jsonpath='{.data.ca\.crt}')
```

- Generate kubeconfig
```
mac$ echo "
apiVersion: v1
kind: Config
clusters:
- name: ${NAMESPACE}-cluster
  cluster:
    certificate-authority-data: ${CA}
    server: ${APISERVER}
contexts:
- name: ${NAMESPACE}-context
  context:
    cluster: ${NAMESPACE}-cluster
    namespace: ${NAMESPACE}
    user: ${NAMESPACE}-user
current-context: ${NAMESPACE}-context
users:
- name: ${NAMESPACE}-user
  user:
    token: ${TOKEN}
" > $NAMESPACE.kubeconfig
```
This will generate file name `production.kubeconfig`.

- Copy `production.kubeconfig` to gitlab-runner-vm and place in `.kube/config` in `gitlab-runner` user.
```
mac$ ssh vagrant@192.168.150.10
```

```
vagrant$ sudo su
root# su gilab-runner
gitlab-runner$ mkdir ~/.kube
gitlab-runner$ vi ~/.kube/config
```

- Check gitlab-runner-vm can access Kubernetes
```
kubectl get pod -n production
```

#### CI & CD Setup
- create environment variables in GitLab (Settings > CI/CD > Variables)

| key | value |
|-|-|
| DOCKER_USERNAME | docker hub username |
| DOCKER_PASSWORD | docker hub password |

make sure all of the variables is masked

- change apps code and push to repository, the CI/CD will automatically run.
- to access the application
```
minikube service alitwitter-service -n production
```
